import unittest
from selenium import webdriver

class TestGoogleHomePage(unittest.TestCase):

	def setUp(self):
		self.browser = webdriver.Firefox()
	def testTitle(self):
		self.browser.get('http://www.google.com')
		self.assertIn('Google', self.browser.title)
	def tearDown(self):
		self.browser.quit()

if __name__ == '__main__':
	unittest.main(verbosity=2)

#browser = webdriver.Firefox()
#browser.get('http://www.google.com/')
